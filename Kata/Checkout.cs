﻿
namespace Kata
{
    using System.Collections.Generic;
    using System.Linq;
    using Model;

    public class Checkout
    {
        private readonly List<Item> _basket = new List<Item>();
        private readonly IBasketLineTotalCalculator _lineTotalCalculator;
        private readonly IBasketTotalCalculator _totalCalculator;

        public Checkout(
            IBasketLineTotalCalculator lineTotalCalculator,
            IBasketTotalCalculator totalCalculator)
        {
            _lineTotalCalculator = lineTotalCalculator;
            _totalCalculator = totalCalculator;
        }

        public decimal Total()
        {
            List<BasketLine> basketLines = _basket.GroupBy(item => item.SKU)
                .Select(lineItem => _lineTotalCalculator.Calc(lineItem.First(), lineItem.Count()))
                .ToList();

            BasketTotal basketTotal = _totalCalculator.Calc(basketLines);

            return basketTotal.Total;
        }

        public void Scan(Item item)
        {
            _basket.Add(item);
        }
    }
}
