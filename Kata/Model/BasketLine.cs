﻿
namespace Kata.Model
{
    public class BasketLine
    {
        public Item Item { get; }
        public int Quantity { get; }
        public decimal NoDiscountTotal { get; }
        public decimal Discount { get; }
        public decimal LineTotal { get; }
        public string DiscountDescription { get; }

        public BasketLine(
            Item item,
            int quantity,
            decimal noDiscountTotal,
            decimal discount,
            decimal lineTotal,
            string discountDescription)
        {
            Item = item;
            Quantity = quantity;
            NoDiscountTotal = noDiscountTotal;
            Discount = discount;
            LineTotal = lineTotal;
            DiscountDescription = discountDescription;
        }
    }
}
