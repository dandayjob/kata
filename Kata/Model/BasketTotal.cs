﻿
namespace Kata.Model
{
    public class BasketTotal
    {
        public decimal NoDiscountTotal { get; }
        public decimal Discount { get; }
        public decimal Total { get; }
        public string DiscountDescription { get; }

        public BasketTotal(
            decimal noDiscountTotal,
            decimal discount,
            decimal total,
            string discountDescription)
        {
            NoDiscountTotal = noDiscountTotal;
            Discount = discount;
            Total = total;
            DiscountDescription = discountDescription;
        }
    }
}
