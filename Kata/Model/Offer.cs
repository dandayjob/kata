﻿
namespace Kata.Model
{
    public class Offer
    {
        public string SKU { get; }
        public int Quantity { get;  }
        public decimal OfferPrice { get; }
        public string Description { get; }

        public Offer(string sku, int quantity, decimal offerPrice, string description)
        {
            SKU = sku;
            Quantity = quantity;
            OfferPrice = offerPrice;
            Description = description;
        }
    }
}
