﻿
namespace Kata
{ 
    using System.Collections.Generic;

    public interface IBasketTotalCalculator
    {
        Model.BasketTotal Calc(List<Model.BasketLine> basketLines);
    }
}
