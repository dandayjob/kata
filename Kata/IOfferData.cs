﻿
namespace Kata
{
    public interface IOfferData
    {
        Model.Offer BySKU(string SKU);
    }
}
