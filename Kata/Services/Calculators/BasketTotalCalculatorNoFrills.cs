﻿
namespace Kata.Services.Calculators
{
    using System.Collections.Generic;
    using System.Linq;
    using Kata.Model;

    public class BasketTotalCalculatorNoFrills : IBasketTotalCalculator
    {
        public BasketTotal Calc(List<BasketLine> basketLines)
        {
            return new BasketTotal(
                noDiscountTotal: basketLines.Sum(line => line.NoDiscountTotal),
                discount: basketLines.Sum(line => line.Discount),
                total: basketLines.Sum(line => line.LineTotal),
                discountDescription: string.Empty);
        }
    }
}
