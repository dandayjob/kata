﻿
namespace Kata.Services.Calculators
{
    using Kata.Model;

    public class BasketLineTotalCalcWithBasicOffers : IBasketLineTotalCalculator
    {
        private readonly IOfferData _offers;

        public BasketLineTotalCalcWithBasicOffers(IOfferData offers)
        {
            _offers = offers;
        }

        public BasketLine Calc(Item item, int quantity)
        {
            Offer offer = _offers.BySKU(item.SKU);
            decimal noDiscountTotal = item.UnitPrice * quantity;

            if (offer == null)
            {
                return new BasketLine(
                    item: item,
                    quantity: quantity,
                    noDiscountTotal: noDiscountTotal,
                    discount: 0,
                    lineTotal: item.UnitPrice * quantity,
                    discountDescription: string.Empty);
            }

            decimal lineTotal = CalculateTotalWithOffer(item, quantity, offer);

            return new BasketLine(
                item: item,
                quantity: quantity,
                noDiscountTotal: noDiscountTotal,
                discount: noDiscountTotal - lineTotal,
                lineTotal: lineTotal,
                discountDescription: offer.Description);
        }

        private decimal CalculateTotalWithOffer(Item item, int quantity, Offer offer)
        {
            int offerQuantity = quantity / offer.Quantity;
            int remainingQuantity = quantity % offer.Quantity;

            return offer.OfferPrice * offerQuantity
                + item.UnitPrice * remainingQuantity;
        }
    }
}
