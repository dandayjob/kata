﻿
namespace Kata
{
    public interface IBasketLineTotalCalculator
    {
        Model.BasketLine Calc(Model.Item item, int quantity);
    }
}
