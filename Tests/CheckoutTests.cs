
namespace Tests
{
    using System.Linq;
    using FluentAssertions;
    using Kata;
    using Kata.Model;
    using Kata.Services.Calculators;
    using Moq;
    using Xunit;

    public class CheckoutTests
    {
        private Item _testItem1 = new Item ( sku: "A99", unitPrice: 0.5m );
        private Item _testItem2 = new Item ( sku: "B15", unitPrice: 0.3m );
        private Item _testItem3 = new Item ( sku: "C40", unitPrice: 0.6m );

        private Offer[] _testOffers =
        {
            new Offer ( sku: "A99", quantity: 3, offerPrice: 1.3m, description: "" ),
            new Offer ( sku: "B15", quantity: 2, offerPrice: 0.45m, description: "" )
        };

        [Fact]
        public void CanScanItem()
        {
            Mock<IOfferData> offerDataMock = new Mock<IOfferData>();

            BasketLineTotalCalcWithBasicOffers lineTotalCalculator
                = new BasketLineTotalCalcWithBasicOffers(offerDataMock.Object);
            BasketTotalCalculatorNoFrills basketTotalCalculator
                = new BasketTotalCalculatorNoFrills();

            Checkout checkout = new Checkout(lineTotalCalculator, basketTotalCalculator);

            checkout.Scan(_testItem1);
            checkout.Scan(_testItem2);
            checkout.Scan(_testItem3);
        }

        [Fact]
        public void CanRequestTotalPrice()
        {
            Mock<IOfferData> offerDataMock = new Mock<IOfferData>();

            BasketLineTotalCalcWithBasicOffers lineTotalCalculator
                = new BasketLineTotalCalcWithBasicOffers(offerDataMock.Object);
            BasketTotalCalculatorNoFrills basketTotalCalculator
                = new BasketTotalCalculatorNoFrills();

            Checkout checkout = new Checkout(lineTotalCalculator, basketTotalCalculator);

            checkout.Scan(_testItem1);
            checkout.Scan(_testItem2);
            checkout.Scan(_testItem3);

            checkout.Total().Should().Be(1.4m);
        }

        [Fact]
        public void CanAccountForOffers()
        {
            Mock<IOfferData> offerDataMock = new Mock<IOfferData>();
            offerDataMock.Setup(moq => moq.BySKU(It.IsAny<string>()))
                .Returns<string>(lookupOffer);

            BasketLineTotalCalcWithBasicOffers lineTotalCalculator
                = new BasketLineTotalCalcWithBasicOffers(offerDataMock.Object);
            BasketTotalCalculatorNoFrills basketTotalCalculator
                = new BasketTotalCalculatorNoFrills();

            Checkout checkout = new Checkout(lineTotalCalculator, basketTotalCalculator);

            checkout.Scan(_testItem2);
            checkout.Scan(_testItem1);
            checkout.Scan(_testItem2);

            checkout.Total().Should().Be(0.95m);
        }

        [Fact]
        public void CanAccountForMultipleOffers()
        {
            Mock<IOfferData> offerDataMock = new Mock<IOfferData>();
            offerDataMock.Setup(moq => moq.BySKU(It.IsAny<string>()))
                .Returns<string>(lookupOffer);

            BasketLineTotalCalcWithBasicOffers lineTotalCalculator
                = new BasketLineTotalCalcWithBasicOffers(offerDataMock.Object);
            BasketTotalCalculatorNoFrills basketTotalCalculator
                = new BasketTotalCalculatorNoFrills();

            Checkout checkout = new Checkout(lineTotalCalculator, basketTotalCalculator);

            checkout.Scan(_testItem2);
            checkout.Scan(_testItem1);
            checkout.Scan(_testItem2);
            checkout.Scan(_testItem1);
            checkout.Scan(_testItem2);
            checkout.Scan(_testItem3);
            checkout.Scan(_testItem1);

            checkout.Total().Should().Be(2.65m);
        }

        private Offer lookupOffer(string SKU)
        {
            return _testOffers.FirstOrDefault(offer => offer.SKU == SKU);
        }
    }
}